import React from 'react';
import ReactDOM from 'react-dom';
import Home from './js/components/Home';
import PercentOfDeaths from './js/components/DoughnutChart';
import DeathsByAge from './js/components/BarChart';
import ChanceOfDeath from './js/components/ChanceOfDeath';
import {BrowserRouter as Router,Switch,Route} from 'react-router-dom'
ReactDOM.render(
  <React.StrictMode>
    <Router>
        <Switch>
          <Route exact path = "/covid-deaths-age">
            <DeathsByAge></DeathsByAge>
          </Route>
          <Route exact path = "/covid-deaths-chance">
            <ChanceOfDeath></ChanceOfDeath>
          </Route>
          <Route exact path = "/covid-deaths-share">
            <PercentOfDeaths></PercentOfDeaths>
          </Route> 
          <Route path = '*'>
            <Home></Home>  
          </Route>                 
        </Switch>
      </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// Use the render props in the routes to pass the params to the components