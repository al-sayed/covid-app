import '../../css/base.css'
import '../../css/App.css';
import bar_graph from './res/icons/bar-chart-outline-2.svg';
import people from './res/icons/people-group.svg';
import pie_graph from './res/icons/ios-pie-outline-2.svg';
import DeathsByAgeGraph from '../js/components/BarChart';
import PercentDeathsGraph from '../js/components/DoughnutChart';
import ChanceOfDeath from '../js/components/ChanceOfDeath';
import React from 'react';
import {useState,useEffect} from 'react';
import {generateAgeData,generateConditionData,test} from './truth';
import {Route,Switch,Link} from 'react-router-dom';
// add conditional rendering here to make sure the data arrives before you draw the chart.

function App() {
  const [dataLoaded,setDataLoaded] = useState(false);
  const [ageData,setAgeData] = useState('');
  const [conditionData,setConditionData] = useState('');
  const [testMode,setTestMode] = useState(true);
  
  // const [pieLoaded,setPieLoaded] = useState(null);
  const [pieData, setPieData] = useState(null);
  useEffect(()=>{
    fetchCSV();

  },[]);

  async function fetchCSV(){
    try {
      const {conditionData,ageData,percentData} = await test();
      
      setConditionData(conditionData);
      setAgeData(ageData);
      setPieData(percentData)
      setDataLoaded(true);
     // console.log(conditionData);
      // console.log(ageData);
      // console.log(percentData);
    } catch (e) {
      console.error(e.customMsg+"\n\n",e);
      setDataLoaded(false);
    }
  }

  if(!testMode) {
  return (
    <div className="App">
      {/* <CovidGraph></CovidGraph> */}
      <h1 className="text-center">How Deadly is Covid?</h1>
      <nav className = "">
        <div className="nav-box">
          <a href = "#"className="nav-icon">
            <img src={bar_graph} alt = "a bar graph" className="filter"></img>
            <span className = "nav-label">Infections v Deaths</span>
          </a>
        </div>
        <div className="nav-box">
          <a href = "#"className="nav-icon">
            <img src={people} alt = "a group of people icon" className="filter"></img>
            <span className = "nav-label">Chance of Death</span>
            </a>

        </div>
        <div className="nav-box">
          <a href = "#"className="nav-icon">
            <img src={pie_graph} alt = "a pie graph" className="filter"></img>
            <span className = "nav-label">Deaths by Age</span>
          </a>
        </div>
      </nav>
    </div>
  );
  }

  const graphTest = true;
  return(

    <div className="App">
      {/* {graphTest && <h1 className = "text-center">Test Mode</h1>}
      {dataLoaded && <ChanceOfDeath  {...conditionData}></ChanceOfDeath>} */}
      
      
      {graphTest && dataLoaded ? 
          <>
            <DeathsByAgeGraph {...ageData}></DeathsByAgeGraph>
          </>
          : <h1 className = "text-center">Loading</h1>
          }
     
                {/* {dataLoaded ?
                <>
                   <PercentDeathsGraph {...pieData}></PercentDeathsGraph> 
                </>
                  : <h1 className = "text-center">Loading</h1>
               } */}

    </div>
  );

  
}

export default App;
