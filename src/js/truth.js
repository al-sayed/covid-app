
//data source from : https://github.com/nychealth/coronavirus-data/tree/00c95eb19c04c85b31f7efc70200ff725ab92e6c
import ageCSV from '../res/data/by-age.csv';
import conditionCSV from '../res/data/deaths-by-underlying-conditions.csv';
import AbortController from 'node-abort-controller';

  export async function generateAllData(timeout) {
      const b = await generateAgeData(timeout);
      const {ageGroupInfections,totalDead,ageGroupDeaths,ageGroups} = b;
      const a = await generateConditionData(ageGroupInfections,timeout);
      const agePercentDeaths = generatePercentData(totalDead,ageGroupDeaths);
      const percentData = {agePercentDeaths,ageGroups}
      return {ageData:b,chanceData:a,percentData}

  }

  export async function generateConditionData(ageGroupInfections,timeout) {
    try {
      const controller = new AbortController(); 
      const cancelSlowRequest = setTimeout(()=> controller.abort,timeout);
      const text = await(await fetch(conditionCSV,{signal:controller.signal})).text();
      clearTimeout(cancelSlowRequest);
      const labels = extractLabels(text).slice(1,4);
      let data = extractAgesAndData(text,3);
      let deathChanceSick;
      let deathChanceHealthy;

      let deathFraction = {};
      const groups = data.ageGroups;
      
      data.ageData.forEach( (valueArr,index)=> {
        deathChanceSick = +(1/(valueArr[1]/ageGroupInfections[index])).toFixed();
        deathChanceHealthy = +(1/(valueArr[2]/ageGroupInfections[index])).toFixed();
        deathFraction[groups[index]] ={deathChanceSick,deathChanceHealthy};
        });
      data = {...data,deathFraction};
      return {...data, labels}
    } catch(e) {
      console.log(e);
      e.customMsg = "Could not read condition data from source csv.";
      throw e
    }
  }
  function extractAgesAndData(text,dataColumnsWanted = undefined) {
      if(dataColumnsWanted)
        dataColumnsWanted+=1; //accounts for slicing off the first element as the age

      const ageDataReg = /(?<age_data>^(\d+(-\d+|\+)).*(\d+)(?:,)?$)/gim;
      const ageDataRes = text.matchAll(ageDataReg);
      const ageGroups = [];
      const ageData = [];
      for(const match of ageDataRes) {
        let {age_data} = match.groups;
        if(age_data) {      
          let temp = age_data.split(',');
          let ageGroup = temp[0];
          ageGroups.push(ageGroup);
          ageData.push(temp.slice(1,dataColumnsWanted));
        }
      }
      return {ageGroups,ageData};

  }
   


export async function generateAgeData(timeout) {
  try{
    const controller = new AbortController();
    const cancelSlowRequest = setTimeout(()=> controller.abort,timeout);
    const data = await (await fetch(ageCSV,{signal:controller.signal})).text();
    clearTimeout(cancelSlowRequest);
   //combine the age and value regex so all the age and values are all in one capturing group

   let ageGroupDeaths = [];
   let ageGroupInfections = [];
   let ageGroupHospitalized = [];
   let ageGroups = [];
   let totalDead = 0;
   let totalInfected = 0;
   let totalHospitalized = 0;
       
   //captures the age group, and all numbers corresponding to the label fields

   const ageDataReg = /(?<age_data>^(\d+(-\d+|\+)).*(\d+)(?:,)?$)|(?<totals>^citywide.+\d+?$)/gim; //see explanation at the page end
   const ageDataRes = data.matchAll(ageDataReg);
   const toFixArr = [];
   let counter = 0;
   for(const match of ageDataRes) {
        let {age_data,totals} = match.groups;

        if (age_data) {
          let temp = age_data.split(',');
          let ageGroup = temp[0];

          //this collects all 4 under-18 age categories to correct and reconsilidate into one group
          if(counter < 4) {
            toFixArr.push(temp);
            counter++;
            continue;
          }
            ageGroups.push(ageGroup);  

            let dead = +temp[temp.length-1];
            let hospitalized = +temp[temp.length-2];
            let infected = +temp[temp.length-3];
 
            ageGroupDeaths.push(dead);
            ageGroupInfections.push(infected);
            ageGroupHospitalized.push(hospitalized);

        } else if(totals) {
          let temp = totals.split(',');
          const values = temp.slice(-3);
          totalDead = values[2];
          totalHospitalized = values[1];
          totalInfected = values[0]
        }
   }

   const under18 = correctUnder18Data(toFixArr);
   const under18Infected = under18[1];
   const under18Hospitalized = under18[2];
   const under18Dead= under18[3];
   const under18AgeGroup = under18[0];


   ageGroups = [under18AgeGroup, ...ageGroups];
   ageGroupInfections = [under18Infected, ...ageGroupInfections];
   ageGroupHospitalized = [under18Hospitalized, ...ageGroupHospitalized];
   ageGroupDeaths = [under18Dead, ...ageGroupDeaths];

   //LABEL VALUES:
////AGE_GROUP,CASE_RATE,HOSPITALIZED_RATE,DEATH_RATE,CASE_COUNT,HOSPITALIZED_COUNT,DEATH_COUNT
//   n_RATE means it's per 100k people


    const finalData = {
      ageGroups,ageGroupInfections, ageGroupHospitalized,
      ageGroupDeaths, totalInfected, totalHospitalized, totalDead};

    return finalData;

    } catch(e) {
      e.customMsg = "Could not read age data from source csv.";
      throw e
    }
  }

function extractLabels(text) {

   const labelReg = /(?<label>([a-z]+(?:_?))+)/gi //*
   const labelRes = text.matchAll(labelReg);
   const labels = [];
      for(const match of labelRes) {
        let {label} = match.groups;
        if (label) {
          labels.push(label);
        }
   }
   return labels;
   /* matches all input case-insensitively
   
   1. captures any letter one or more times then matches but doesn't capture an optional underscore,
   2.  The result of 1. is repeated captured into the label named capturing group
   */
}


function correctUnder18Data(arr) {
  //The under18 data split into multiple arrays, and this consolidates them.
  let sick = 0;
  let hospital = 0;
  let dead = 0;
  arr.forEach(e=>{
    let vals = e.slice(-3);
    sick += vals[0] ? +vals[0] : 0;
    hospital += vals[1] ? +vals[1] : 0;
    dead += vals[2] ? +vals[2] : 0;

  });
  return ["0-17",sick,hospital,dead];

}
   //LABEL VALUES:
////AGE_GROUP,CASE_RATE,HOSPITALIZED_RATE,DEATH_RATE,CASE_COUNT,HOSPITALIZED_COUNT,DEATH_COUNT
//   n_RATE means it's per 100k people

function generatePercentData(totalDead,ageGroupDeaths) {
  const agePercentDeaths = ageGroupDeaths.map((current)=>{
    return +(100*current/totalDead ).toFixed(1); 
  })
  return agePercentDeaths;
}



   /*
   ************REGEX EXPLANATION***************

  /(?<age_data>^(\d+(-\d+|\+)).*(\d+)(?:,)?$)|(?<totals>^citywide.+\d+?$)/gim
   
  (?<age_data>^(\d+(-\d+|\+)).*(\d+)(?:,)?$)
    at line start, captures one or more digits, a dash and one or more digits, or a + sign
    then 0 or more of any character (all the data) before an optional with a non-capturing comma (,) at the line end

    into the age_data named group

       OR (|)

  (?<totals>^citywide.+\d+?$)
    at line start, captures citywide,then 1 or more of any character (all of the data), 
    then optionally one or more digits at the end of the line.

    into the totals named group

    
    the regex will parse the entire input case insensitively
    */