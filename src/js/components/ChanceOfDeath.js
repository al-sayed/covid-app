import React from 'react'
import  '../../css/base.css';
// import '../../ChanceOfDeath.css';
import styles from '../../css/ChanceOfDeath.module.css';
import {useState,useRef} from 'react';
import {useLocation,Link,Redirect} from 'react-router-dom';
export default function ChanceOfDeath() {
    const ageSelector = useRef(null);
    const [healthy,setHealthy] = useState(true);

    const healthyData = []
    const sickData = []
    const [dataIndex,setDataIndex] = useState(0);
    const [showAddendum,setShowAddendum] = useState(false);

    const location = useLocation();

    let readStorage = false;
    if(!location.state || Object.keys(location.state).length === 0) {
        if(!localStorage.length) {
          return <Redirect to={ { pathname:"/", state:{ from: location.pathname} } }></Redirect>
        }
        readStorage = true;
    }    
    

    const storage = localStorage;
    const {ageGroups:ageRange,deathFraction} = !readStorage ? location.state : JSON.parse(storage.getItem('chanceData'));

    Object.values(deathFraction).forEach(value => {
        sickData.push(value.deathChanceSick); healthyData.push(value.deathChanceHealthy)
     } );

   function handleAgeSelect() {
       const newIndex = ageSelector.current.selectedIndex;
       setDataIndex( newIndex ); 
       (newIndex > 5 && healthy) ? setShowAddendum(true):setShowAddendum(false);

   }
   function handleHealthyToggle () {
       if(healthy) {
           setShowAddendum(false);
           //addendum only applies to the healthy values
       }

       if(!healthy && dataIndex > 5) {
           setShowAddendum(true);
       }
       
       setHealthy(!healthy);
   }
    return (
        
        <div className={styles['data-container']}>
            <h3>Covid-19 Kills</h3> 
                <h2 className={styles['main-data']}>
                    1 in {healthy ? healthyData[dataIndex]:sickData[dataIndex]}
                    {showAddendum && healthy && <span className={styles['asterisk']}>*</span>}</h2>
            <div className = {styles['data-control']}>
                {/* {console.log(styles)} */}
                <button className={`${styles['toggle-healthy-btn']} ${styles['btn']}`} onClick={handleHealthyToggle}>
                {<span style={{color:`${healthy? 'green':'red'}`}}>{healthy? 'Healthy':'Sick'}</span>}
                </button>
                <div className={`${styles['select-wrapper']} ${ healthy ? styles['select-wrapper--healthy']:styles['select-wrapper--sick']}`}>
                <select className={`${styles['select-age-btn']} ${styles['btn']}`} ref={ageSelector} onChange={ handleAgeSelect}>
                    {ageRange.map((age,index)=>{
                        return( <option key={index}value ={index}>{age}</option>);
                    })}
                </select>
                </div>
                <h4>year olds</h4>
            </div>
            {showAddendum && 
            <div className={styles['addendum']}>
                
                    *The death rate should be increasing with age.
                    This implies errors in the data collected on the elderly 
                    by the New York City Health Department.
            </div>}
            <Link to="/" className={`${styles['return-home-btn']} ${styles['btn']}`}>Home</Link>

        </div>
    )
}
