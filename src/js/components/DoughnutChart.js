import React from 'react';
import {useEffect, useRef} from 'react';
import {Doughnut} from 'react-chartjs-2';
import '../../css/base.css';
import {useLocation, Link, Redirect} from 'react-router-dom';
// import container from '../../css/DoughnutChart.module.css';
// import container from '../../css/graphs.module.css';
import styles from '../../css/DoughnutChart.module.css';

const newData = [];
const data = {
  labels: [],
  datasets: [
    {
      label: 'Deaths by Age',
      data: newData,
      backgroundColor: [

        // 'hsl(0,100%,50%)',
        // "hsl(30,100%,50%)",
        // " hsl(60,100%,50%)",
        //  "hsl(120,100%,50%)",
        //  "hsl(180,100%,50%)",
        //  "hsl(240,100%,50%)",
        //  "hsl(270,100%,50%)",
        //  "hsl(300,100%,50%)",

        "hsl(300,100%,50%,100%)",
        "hsl(270,100%,50%,100%)",
        "hsl(240,100%,50%,100%)",
        "hsl(180,100%,50%,100%)",
        "hsl(120,100%,50%,100%)",
        "hsl(60,100%,50%,100%)",
        "hsl(30,100%,50%,100%)",
        "hsl(0,100%,50%,100%)",
      ],
      borderColor: [
        'rgba(255, 99, 132, 1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)',
      ],
      borderWidth: 1,
    },
  ],
};
//remove the plugins later on
const options = {
  maintainAspectRatio : true,
    tooltips: {
      displayColors:false,
      bodyFont:{size:17},
      titleFont:{size:20},

      callbacks:{

        backgroundColor: function(x) {
          console.log(x);
        },

        label: function(item, everything) {
          const {index} = item;
        return data.labels[index];
        },

        labelColor:function(item) {
          const {index,datasetIndex} = item;
          const sliceColor = data.datasets[datasetIndex].backgroundColor[index] || 'pink'; //error color

          return {
            backgroundColor:sliceColor
          }
        },

        title:function(item) {
          const {index,datasetIndex} = item[0];  //I don't why know this callback returns the item object in an array         

            const a = +(data.datasets[datasetIndex].data[index]) + '%' || 'wrong'; //error text

          return a;
        },
      }

    },
    
    legend: {
      align:"center",
      position:"top",
      display:true,
      labels: {
        fontColor:"black",
        boxWidth:10,
        fontSize:16,
      }

    }

  
}


 function responsiveFontSize() {
   const width = window.innerWidth;

   switch(true) {    
     case (width >=1400):
       return 23;
    case (width >=1200):
      return 19;
  

   case (width>=1000):
     return 16;
       
    case (width >=750):
     return 14.5;
        
    case (width >=650):
      return 14;
        
    case (width >=500):
      return 13;
      
     case (width >=300):
      return 12;

    default: {
      return 1;
    }

   }
 }
  function displayLegend({current:{chartInstance}}) {

    if(!chartInstance) {
      return;
    }
    const width = window.innerWidth;
    const display = width > 650? true:false;
    chartInstance.options.legend.display = display;
    return display;
  }

  function resizeLegend({current:{chartInstance}}){     
    chartInstance.options.legend.labels.fontSize = responsiveFontSize();
    chartInstance.options.legend.labels.boxWidth = responsiveFontSize();
  }
    


 function PieChart() {
    const location = useLocation();
    const ref = useRef(undefined);
    const displayFn = displayLegend.bind(null,ref);
    const resizeFn = resizeLegend.bind(null,ref);
    useEffect(()=>{

      if(ref.current) {
        const chart = ref.current.chartInstance;
        const legend = chart.options.legend;
        legend.display = displayFn();
        chart.update();
      }
      window.addEventListener("resize",resizeFn);
      window.addEventListener("resize",displayFn);
      return() => {
        window.removeEventListener("resize",resizeFn);
        window.removeEventListener("resize",displayFn);
      }
    },[])


   let readStorage = false;
    if(!location.state || Object.keys(location.state).length === 0) {
        if(!localStorage.length) {
          return <Redirect to={ { pathname:"/", state:{ from: location.pathname} } }></Redirect>
        }
        readStorage = true;
    }    
    
    const storage = localStorage;
    const {agePercentDeaths,ageGroups} = !readStorage ? location.state  : JSON.parse(storage.getItem('percentData'));


    if(data.labels.length === 0) {
        // populateGraph();
        agePercentDeaths.forEach( e => newData.push( e ) );
        data.labels = ageGroups;
    } 
    return (
        <div className = {styles['graph-container']}style ={{maxWidth:'1340px', minWidth:'300px', margin:'0 auto'}}>
        <h3 className={styles['graph-title']}>Death Burden by Age</h3>
         <Doughnut ref = {ref} data={data} options ={options} />
         <Link to="/" className={`${styles['return-home-btn']} ${styles['btn']}`}>Home</Link>
      </div>

    );
}

export default PieChart;
