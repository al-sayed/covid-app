import React from 'react'
import {useEffect} from 'react'
import {Redirect} from 'react-router-dom'
export default function LoadDataAndReturn(props) {
        const {location, data:{ageData,percentData,chanceData}} = props;
        useEffect(()=>{
          if(window.localStorage) {
            window.localStorage.setItem('updated', Date.now().toString())
            window.localStorage.setItem('ageData',JSON.stringify(ageData));
            window.localStorage.setItem('percentData',JSON.stringify(percentData));
            window.localStorage.setItem('chanceData',JSON.stringify(chanceData));
          }
        },[]);

        const origin = location.state.from;
        let newState;
        if(origin ==="/covid-deaths-age") {
          newState = ageData;
        } else if( origin ==="/covid-deaths-chance") {
          newState = chanceData;
        } else if (origin ==="/covid-deaths-percent") {
          newState = percentData;
        }
        return (
          <Redirect to ={ { pathname:origin, state:{...newState } } }></Redirect>
        );
}
