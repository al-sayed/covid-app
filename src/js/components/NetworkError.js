import React from 'react'


export default function NetworkError({handleNetworkRetry,styles}) {

    
    return (
      <div className={styles['err-retry-fetch']}>
        <div className={styles['err-message-block']}>
          <p className={styles['err-message']}>Unable to retrieve data from the network.</p>
          <button className={`${styles['err-retry-btn']} ${styles['btn']}`} onClick={handleNetworkRetry}>Retry</button>
        </div>
      </div>
    )
}
