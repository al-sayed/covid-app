import React from 'react';
import {useState,useEffect,useRef} from 'react';
import {Link,useLocation} from 'react-router-dom';
// import './src/'
import styles from '../../css/Home.module.css';
import bar_graph from '../../res/icons/bar-chart-outline-2.svg';
import people from '../../res/icons/people-group.svg';
import pie_graph from '../../res/icons/ios-pie-outline-2.svg';
import {generateAllData} from '../truth.js';
import ReturnWithData from './ReturnWithData';
export default function Home() {

  const [dataLoaded,setDataLoaded] = useState(false);
  const [ageData,setAgeData] = useState('');
  const [chanceData,setChanceData] = useState('');
  const [percentData, setPercentData] = useState(null);
  const [networkTimeLimit,setNetworkTimeLimit] = useState(5000);

  const networkErrMessageBlock = useRef(null);
  const errMessageBlock = useRef(null);
  const [failedRequests,setFailedRequests] = useState(0);
  const [networkFailure,setNetworkFailure] = useState(false);
  const [retryNetworkRequest, setRetryNetworkRequest] = useState(false);
  const [retryFailure,setRetryFailure] = useState(false);
  const [storageFailure, setstorageFailure] = useState(false)
  useEffect(()=>{
    fetchCSV();
  },[retryNetworkRequest]);
  
  const location = useLocation();

  function storageAvailable() {
    try {
        var storage = window['localStorage'],
            x = '__storage_test__';
        storage.setItem(x, x);
        storage.removeItem(x);
        return true;
    }
    catch(e) {
        return e instanceof DOMException && (
            // everything except Firefox
            e.code === 22 ||
            // Firefox
            e.code === 1014 ||
            // test name field too, because code might not be present
            // everything except Firefox
            e.name === 'QuotaExceededError' ||
            // Firefox
            e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
            // acknowledge QuotaExceededError only if there's something already stored
            storage && storage.length !== 0;
    }
}

  async function fetchCSV(){
  
      try {
        if(!storageAvailable()) {
          throw  {
            name:"LocalStorageNotAccessible",
            message:"Please Enable Cookies to use this Website"
          }
        }
      } catch (e) {
        console.log(e);
        setstorageFailure(true);
        return;
      }

    if(localStorage) {
      const lastUpdated = localStorage.getItem("updated");
      if(lastUpdated) {
          const msPerDay = 86400000;
          const freshData = lastUpdated + msPerDay > Date.now();
          if(freshData) {     
            setChanceData( JSON.parse( localStorage.getItem( "chanceData" ) ) );
            setAgeData( JSON.parse( localStorage.getItem( "ageData" ) ) );
            setPercentData( JSON.parse( localStorage.getItem( "percentData" ) ) );
            setDataLoaded(true);
            return;
          }
      }
    } else if(dataLoaded) {
      return;
    }

    try {
      const {chanceData,ageData,percentData} = await generateAllData(networkTimeLimit);
      
      setNetworkFailure(false);
      setChanceData(chanceData);
      setAgeData(ageData);
      setPercentData(percentData);
      setDataLoaded(true);

      window.localStorage.setItem('updated', Date.now().toString());
      window.localStorage.setItem('ageData',JSON.stringify(ageData));
      window.localStorage.setItem('percentData',JSON.stringify(percentData));
      window.localStorage.setItem('chanceData',JSON.stringify(chanceData));

    } catch (e) { 
      console.error(e.customMsg+"\n\n",e);
      if(e.name !== 'LocalStorageError') {
        setstorageFailure(true);
      }
        setFailedRequests(prevState=> prevState+1);
        setNetworkFailure(true);
        if(networkFailure) {
          setRetryFailure(true);
        }
      setDataLoaded(false);
    }

  }


    useEffect(()=>{
      if(failedRequests >1) {
        const {current:msgBlock} = networkErrMessageBlock;
        const errBtn = msgBlock.getElementsByTagName("button")[0];
        const errorText = msgBlock.getElementsByTagName("p")[0];
        errBtn.style.display = 'none';
        errorText.style.visibility = "visible";
      }
    },[failedRequests])


   function handleNetworkRetry (event) {

    const errorBtn = event.target;
    const errorText= event.target.previousElementSibling;
    errorText.style.visibility="hidden";
     
    errorBtn.disabled = true;
    errorBtn.style.backgroundColor="grey";
    errorBtn.style.color="black";
    errorBtn.style.marginBottom="6rem";
    errorBtn.textContent="Retrying...";

     setNetworkTimeLimit(10000);
    //refreshes the current page;
    setRetryNetworkRequest((prevState)=>!prevState);
  }

  function reloadPage() {
    window.location = "/";
  }
  const redirectedToHomepage = location.state;

  if(!dataLoaded && !networkFailure && !storageFailure) {
    return(
      <div className={`${styles['App']} ${styles['animated-background']} `}>
        <h2 className={ `${styles['main-text']} ${styles['center-on-page']}`}>Loading...</h2>
      </div>
    )
  }
  return (
    <div className={styles['App']}>

      { redirectedToHomepage && 
        dataLoaded && 
        <ReturnWithData location = {location} data={{chanceData:chanceData,ageData:ageData,percentData:percentData}}></ReturnWithData>
      }
      
      <h1 className={`${styles['text-center']} ${styles['main-text']}`}>How Deadly is Covid?*</h1>
      { networkFailure && 
          <div className={styles['err-container']}>
            <div className={styles['err-message-block']} ref={ networkFailure ? networkErrMessageBlock : null}>
              {retryFailure ?
                <React.Fragment>
                  <p className={styles['err-message']}> The follow-up request didn't go through.<br></br>Do you have a connection?</p> 
                  <button className={styles['btn']} style={{backgroundColor:'red'}} onClick={reloadPage }>Reload</button>
                </React.Fragment>
               :
                  <p className={styles['err-message']}>Unable to retrieve data from the network.</p> 

              }
              <button className={`${styles['err-retry-btn']} ${styles['btn']}`} onClick={handleNetworkRetry}>
                Retry
              </button>
            </div>
          </div>   
      }
      { storageFailure && 
          <div className={styles['err-container']}>
            <div className={styles['err-message-block']} ref={ storageFailure ? errMessageBlock : null}>
                  <p className={styles['err-message']}> Please Enable Cookies to use this Website</p> 
                  <button className={styles['btn']} style={{backgroundColor:'red'}} onClick={reloadPage }>Reload</button>
            </div>
          </div>   
      }      
      <nav className = "">
        <div className={styles['nav-box']}>
          <Link to = {{ pathname:"/covid-deaths-age", state:{ ...ageData }} } className={`${styles['nav-icon']} ${!dataLoaded && styles['nav-disabled']}` }>
            <img className = {styles['image']}src={bar_graph} alt = "a bar graph icon" ></img>
            <span className = {styles['nav-label']}>Deaths by Age</span>
          </Link>
        </div>
        <div className={styles['nav-box']}>
          <Link to = {{ pathname:"/covid-deaths-chance", state:{ ...chanceData }} } className={`${styles['nav-icon']} ${!dataLoaded && styles['nav-disabled']}` }>
            <img className = {styles['image']}src={people} alt = "a group of people icon" ></img>
            <span className = {styles['nav-label']}>Chance of Death</span>
            </Link>
        </div>
        <div className={styles['nav-box']}>
          <Link to = {{ pathname:"/covid-deaths-share", state:{ ...percentData }} } className={`${styles['nav-icon']} ${!dataLoaded && styles['nav-disabled']}` }>
            <img className = {styles['image']}src={pie_graph} alt = "a group of people icon" ></img>
            <span className = {styles['nav-label']}>Chance of Death</span>
            </Link>
        </div>
        <a className={styles['disclaimer-source-link']}href="https://github.com/nychealth/coronavirus-data/tree/00c95eb19c04c85b31f7efc70200ff725ab92e6c" rel="noopener" target="_blank">*In New York City</a>
      </nav>
      <div className={styles['animated-background']}>
        <ul>
          <li className={styles['moving-figures']}></li>
          <li className={styles['moving-figures']}></li>
          <li className={styles['moving-figures']}></li>
          <li className={styles['moving-figures']}></li>
          <li className={styles['moving-figures']}></li>
        </ul>
      </div>
    </div>
  );
}
