import React from 'react';
import { Bar } from 'react-chartjs-2';
import '../../css/base.css';
import container from '../../css/graphs.module.css';
import styles from '../../css/DeathsByAgeBar.module.css';
import {useLocation, Link,Redirect} from 'react-router-dom';

const deaths = [];
const labels = [];

const data = {
  labels: labels,
  datasets: [
    {
      label:'Dead',
      data:deaths,
      backgroundColor: 'rgb(255,0,0)',
    },
  ],
};

const options = {
  tooltips:{
    backgroundColor:'hsl(0,100%,95%)',
    displayColors:false,
    bodyFont:{size:17},
    callbacks: {
      title:function(item) {

      },
      label:function(item) {
        console.log(item);
        return item.value + " Dead";                  
      },
      labelTextColor:function(){
        return 'red';
      },
      labelColor:function() {
        return {
          borderColor:'white',
          backgroundColor:'orange'
        }
      },
      footer:function(){
        return ;
      }
    }
  },
  legend: {
    display:false,
  },
  scales: {
        xAxes:[ 
          {
          ticks: {
              fontColor:'hsla(0,0%,25%,1)',
              fontSize: 16,
              fontStyle:"bold",
              },
          },
        ],
        yAxes: [
          {
            ticks: {
              fontColor:'hsla(0,0%,25%,1)',
              fontSize: 16,
              fontStyle:"bold",
            },
        },
      ]
  }
}

const graphAddendum = [];
export default function SickVsDeadBar() {
         const location = useLocation();
         let readStorage = false;
    if(!location.state || Object.keys(location.state).length === 0) {
        if(!localStorage.length) {
          return <Redirect to={ { pathname:"/", state:{ from: location.pathname} } }></Redirect>
        }
        readStorage = true;
    }    
    
    const storage = localStorage;
    const {ageGroups,ageGroupDeaths} =  !readStorage ? location.state : JSON.parse(storage.getItem('ageData'));

        function populateGraph(){
          ageGroupDeaths.forEach(x => deaths.push(x));
          ageGroups.forEach((x,index) => {
            const toAdd = ageGroupDeaths[index] < 300 ? x+"*".repeat(graphAddendum.length+1):x;
            labels.push(toAdd);
            if(toAdd.endsWith("*")) {
              graphAddendum.push(index);
            }
          });
          //write the below threshold values in an object that you add to the graph.
        }

        if(data.labels.length ===0) {
          populateGraph();
        }

    return(
      <article className ={` ${styles['graph-container']}` }>
        <h3 className={container['graph-title']}>Deaths By Age</h3>
        <Bar data={data} options={options} />

        {graphAddendum.length > 0 &&
        <div className={styles['graph-extras']}>
          {graphAddendum.map((ele)=>{
            return(
              <p className = {styles['graph-extras p']}key={labels[ele]}>{`${deaths[ele]} ${labels[ele].replace(/[^*]/g,"")}`}</p>
              );
          })}
        </div>}

          <Link to="/" className={`${styles['return-home-btn']} ${styles['btn']}`}>Home</Link>
      </article>
    );
}